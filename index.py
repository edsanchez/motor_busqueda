import os
import gevent
import gevent.monkey

gevent.monkey.patch_all()

import bottle
from bottle import route, run
from pymongo import MongoClient
from bson.son import SON


bottle.debug(True)

cliente = MongoClient('mongodb://edsanchez:ml*edder*1008@ds041432.mongolab.com:41432/chefcito')
db = cliente.chefcito

@route('/<ingredientes>')
def inicio(ingredientes):
    ingredientes = ingredientes.split(',')
    pipeline = [{
        "$project":{
            "nombre":1,
            "relevancia":{
                "$add": [
                    {"$multiply":[
                        {"$divide": [{"$size":{
                            "$setIntersection": ["$ingredientes.ingrediente",ingredientes]
                        }}, {"$size": "$ingredientes"}]},
                        0.5
                    ]},
                    {"$multiply":[
                        "$rank",0.1
                    ]}
                ]
            }
        }
    },
    {
        "$sort":{
            "relevancia": -1
        }
    },
    {
        "$match":{
            "relevancia":{"$gt":0}
        }
    }]
    result = db.receta.aggregate(pipeline)
    recetas = []
    for receta in result:
        recetas.append(receta)
    return {"recetas": recetas}

#run(server='gevent', host='0.0.0.0', port=os.environ.get("PORT", 5000))
run(server='gevent', host='0.0.0.0', port=os.environ.get("PORT", 5000))
